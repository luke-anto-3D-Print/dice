#################################################################################
#                                                                               #
# Copyright 2016 Luca Antonelli                                                 #
# luke.anto@gmail.com                                                           #
#                                                                               #
# This file is part of Dice 3D                                                  #
#                                                                               #
# Dice 3D is free software: you can redistribute it and/or modify               #
# it under the terms of the GNU General Public License as published by          #
# the Free Software Foundation, either version 3 of the License, or             #
# (at your option) any later version.                                           #
#                                                                               #
# Wine Predictor is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 #
# GNU General Public License for more details.                                  #
#                                                                               #
# You should have received a copy of the GNU General Public License             #
# along with Dice 3D.  If not, see <http://www.gnu.org/licenses/>.              #
#                                                                               #
#################################################################################
$fn=128;
difference(){
    minkowski(){
        intersection(){
          cube(size=10, center=true);
          sphere(r=7.7);
        }
        sphere(r=0.5);
    }
   {
    // Faccia 1
    translate([0,0,6.5]) sphere(r=1.5);
    // Faccia 2
    translate([6.5,2.5,2.5]) sphere(r=1.5);
    translate([6.5,-2.5,-2.5]) sphere(r=1.5);
    // Faccia 3
    translate([-2.5,-6.5,2.5]) sphere(r=1.5);
    translate([0,-6.5,0]) sphere(r=1.5);
    translate([2.5,-6.5,-2.5]) sphere(r=1.5);
    // Faccia 4
    translate([2.5,6.5,2.5]) sphere(r=1.5);
    translate([2.5,6.5,-2.5]) sphere(r=1.5);
    translate([-2.5,6.5,2.5]) sphere(r=1.5);
    translate([-2.5,6.5,-2.5]) sphere(r=1.5);
    // Faccia 5
    translate([-6.5,2.5,2.5]) sphere(r=1.5);
    translate([-6.5,2.5,-2.5]) sphere(r=1.5);
    translate([-6.5,0,0]) sphere(r=1.5);
    translate([-6.5,-2.5,2.5]) sphere(r=1.5);
    translate([-6.5,-2.5,-2.5]) sphere(r=1.5);
    // Faccia 6
    translate([2.5,2.5,-6.5]) sphere(r=1.5);
    translate([2.5,0,-6.5]) sphere(r=1.5);
    translate([2.5,-2.5,-6.5]) sphere(r=1.5);
    translate([-2.5,2.5,-6.5]) sphere(r=1.5);
    translate([-2.5,0,-6.5]) sphere(r=1.5);
    translate([-2.5,-2.5,-6.5]) sphere(r=1.5);

   }
}
